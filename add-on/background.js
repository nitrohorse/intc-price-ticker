'use strict';

function setBadgeBackgroundColor(color) {
    chrome.browserAction.setBadgeBackgroundColor({
        color: color
    });
}

function setBadgeText(text) {
    chrome.browserAction.setBadgeText({
        text: text
    });
}

function setBadgeTitle(title) {
    chrome.browserAction.setTitle({
        title: title
    });
}

function createActiveTabTo(url) {
    chrome.tabs.create({
        active: true,
        url: url
    });
}

function setupPriceFetchInterval() {
    window.setInterval(() => {
        updateBadge();
    }, 600000);
}

function updateBadgeAsInfoNotAvailable() {
    setBadgeBackgroundColor('red');
    setBadgeText('N/A');
}

function getChangeIndicatorFrom(change) {
    let changeIndicator;
    if (change.charAt(0) == "+") {
        changeIndicator = "⬆";
    } else if (change.charAt(0) == "-") {
        changeIndicator = "⬇";
    } else {
        changeIndicator = "|";
    }
    return changeIndicator;
}

function updateBadgeWith(info) {
    setBadgeText(info.price);
    let changeIndicator = getChangeIndicatorFrom(info.change);
    let title = `INTC ${changeIndicator} ${info.price} ${info.change} (${info.change_percentage}%)`;
    setBadgeTitle(title);
}

function getInfoFrom(response) {
    // strip the first 3 characters ("// ")
    response = response.substring(3);
    response = JSON.parse(response);
    return {
        price: response[0]['l_cur'],
        change: response[0]['c'],
        change_percentage: response[0]['cp']
    };
}

function makeRequestForPrice() {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const method = 'GET';
        const url = 'https://finance.google.com/finance/info?q=INTC';

        xhr.open(method, url, true);
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = () => {
            reject({
                status: xhr.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

function updateBadge() {
    makeRequestForPrice().then((response) => {
        updateBadgeWith(getInfoFrom(response));
    })
    .catch((err) => {
        console.error($`Oh no, there was an error! {err.status} {err.statusText}`);
        updateBadgeAsInfoNotAvailable();
    });
}

function setupBadge() {
    setBadgeBackgroundColor('#0071C5');

    setBadgeText('...');

    chrome.browserAction.onClicked.addListener((tab) => {
        createActiveTabTo('https://www.google.com/finance?q=intc');
    });
}

// Run
setupBadge();
updateBadge();
setupPriceFetchInterval();
