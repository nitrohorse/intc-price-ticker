'use strict';

var expect = chai.expect;

describe('background.js', () => {

    describe('setupPriceFetchInterval', () => {
        let spy;

        beforeEach(() => {
            spy = sinon.spy(window, 'updateBadge');
            this.clock = sinon.useFakeTimers();
        });

        afterEach(() => {
            window.updateBadge.restore();
            this.clock.restore();
        });

        it('should fetch the INTC price every 10 minutes', () => {
            setupPriceFetchInterval(spy);
            expect(spy.called).to.equal(false);

            this.clock.tick(300000);
            expect(spy.called).to.equal(false);

            this.clock.tick(600000);
            expect(spy.called).to.equal(true);

        });
    });

    describe('updateBadgeAsInfoNotAvailable', () => {
        beforeEach(() => {
            updateBadgeAsInfoNotAvailable();
        });

        it('should update the badge background color to red', () => {
            sinon.assert.called(chrome.browserAction.setBadgeBackgroundColor);
            sinon.assert.calledWithExactly(chrome.browserAction.setBadgeBackgroundColor, {
                color: 'red'
            });
        });

        it('should update the badge text to "N/A"', () => {
            sinon.assert.called(chrome.browserAction.setBadgeText);
            sinon.assert.calledWithExactly(chrome.browserAction.setBadgeText, {
                text: 'N/A'
            });
        });
    });

    describe('getChangeIndicatorFrom', () => {
        it('should return the indicator corresponding to a price increase', () => {
            let change = '+0.08';
            let expectedIndicator = '⬆';
            let result = getChangeIndicatorFrom(change);
            expect(result).to.equal(expectedIndicator);
        });

        it('should return the indicator corresponding to a price decrease', () => {
            let change = '-0.08';
            let expectedIndicator = '⬇';
            let result = getChangeIndicatorFrom(change);
            expect(result).to.equal(expectedIndicator);
        });

        it('should return the indicator when no change is detected', () => {
            let change = '0.08';
            let expectedIndicator = '|';
            let result = getChangeIndicatorFrom(change);
            expect(result).to.equal(expectedIndicator);
        });
    });

    describe('updateBadgeWith', () => {
        let info = {
            price: '34.72',
            change: '+0.08',
            change_percentage: '0.23'
        };

        it('should update the badge text with the current INTC price', () => {
            updateBadgeWith(info);
            sinon.assert.calledWithExactly(chrome.browserAction.setBadgeText, {
                text: info.price
            });
            sinon.assert.called(chrome.browserAction.setBadgeText);
        });

        it('should update the badge title, indicating an increase in price', () => {
            updateBadgeWith(info);
            let title = `INTC ⬆ ${info.price} ${info.change} (${info.change_percentage}%)`;
            sinon.assert.calledWithExactly(chrome.browserAction.setTitle, {
                title: title
            });
            sinon.assert.called(chrome.browserAction.setTitle);
        });

        it('should update the badge title, indicating an decrease in price', () => {
            info.change = '-0.08';
            updateBadgeWith(info);
            let title = `INTC ⬇ ${info.price} ${info.change} (${info.change_percentage}%)`;
            sinon.assert.calledWithExactly(chrome.browserAction.setTitle, {
                title: title
            });
            sinon.assert.called(chrome.browserAction.setTitle);
        });
    });

    describe('getInfoFrom', () => {
        it('should parse the response and return INTC info', () => {
            const response = '// [ { "l_cur" : "34.72", "c": "+0.08", "cp": "0.23"} ] ';
            const expectedInfo = {
                price: '34.72',
                change: '+0.08',
                change_percentage: '0.23'
            };
            const info = getInfoFrom(response);
            expect(info).to.deep.equal(expectedInfo);
        });
    });

    describe('makeRequestForPrice', () => {
        let stub;

        beforeEach(() => {
            stub = sinon.stub(window, 'makeRequestForPrice');
        });

        afterEach(() => {
            window.makeRequestForPrice.restore();
        });

        it('should fetch the current INTC price', () => {
            const mockResponse = '// [ { "l_cur" : "34.72"} ] ';
            stub.returnsPromise().resolves(mockResponse);
            expect(stub().resolveValue).to.equal(mockResponse);
        });

        it('should throw a 400 error if the request is malformed', () => {
            const mockResponse = { status: 400, statusText: 'Bad Request' };
            stub.returnsPromise().rejects(mockResponse);
            expect(stub().rejectValue).to.equal(mockResponse);
        })
    });

    describe('setupBadge', () => {
        beforeEach(() => {
            setupBadge();
        });

        it('should set the badge background color to "Intel Blue"', () => {
            sinon.assert.calledWithExactly(chrome.browserAction.setBadgeBackgroundColor, {
                color: '#0071C5'
            });
        });

        it('should set the badge text to "..."', () => {
            sinon.assert.called(chrome.browserAction.setBadgeText);
            sinon.assert.calledWithExactly(chrome.browserAction.setBadgeText, {
                text: '...'
            });
        });

        it('should register a listener for onClicked', () => {
            sinon.assert.called(chrome.browserAction.onClicked.addListener);
        });

        it('should open a tab when the button is clicked', () => {
            chrome.browserAction.onClicked.trigger();

            sinon.assert.called(chrome.tabs.create);
            sinon.assert.calledWithExactly(chrome.tabs.create, {
                active: true,
                url: 'https://www.google.com/finance?q=intc'
            });
        });
    });

});
