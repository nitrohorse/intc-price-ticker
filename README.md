[![Build Status](https://travis-ci.org/nitrohorse/intc-price-ticker.svg?branch=master)](https://travis-ci.org/nitrohorse/intc-price-ticker)
[![Coverage Status](https://coveralls.io/repos/github/nitrohorse/intc-price-ticker/badge.svg?branch=master)](https://coveralls.io/github/nitrohorse/intc-price-ticker?branch=master)
[![Greenkeeper badge](https://badges.greenkeeper.io/nitrohorse/intc-price-ticker.svg)](https://greenkeeper.io/)
[![Dependency Status](https://david-dm.org/nitrohorse/intc-price-ticker/dev-status.svg)](https://david-dm.org/nitrohorse/intc-price-ticker?type=dev)
[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://github.com/nitrohorse/intc-price-ticker/blob/master/LICENSE)

# INTC Price Ticker
Is a WebExtension that adds a browser action icon to the toolbar that updates every 10 minutes with the latest Intel Corporation (INTC) stock price. Powered by [Google](https://www.google.com/finance?q=intc). Based on the free and open-source [yabpt browser extension](https://github.com/RockyTV/yabpt).

![alt-text](https://i.imgur.com/KuYpYM0.png)

## Download
* Firefox: ~~https://addons.mozilla.org/en-US/firefox/addon/intc-price-ticker/~~ 
  * Need to find a free, alternative API for real-time INTC stock price. Google's Finance API is deprecated now.

## Install Locally
* Firefox - [temporary](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox)
* Chrome - [permanent](https://superuser.com/questions/247651/how-does-one-install-an-extension-for-chrome-browser-from-the-local-file-system/247654#247654)

## Test Locally
* Clone the repo
* Install npm modules: `yarn` or `npm install`
* Run tests: `yarn run test` or `npm run test`
* Use [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) for faster development: `yarn run web-ext` or `npm run web-ext`
